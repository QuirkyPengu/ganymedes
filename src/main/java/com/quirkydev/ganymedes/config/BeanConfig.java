package com.quirkydev.ganymedes.config;

import com.quirkydev.ganymedes.service.DClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    private static PropertiesConfig propertiesConfig = null;

    public static PropertiesConfig getPropertyConfig() {
        return propertiesConfig;
    }

    @Autowired
    public BeanConfig(PropertiesConfig propertiesConfig) {
        BeanConfig.propertiesConfig = propertiesConfig;
    }

    @Bean
    public DClassService getDofusClassService() {
        return new DClassService();
    }
}
