package com.quirkydev.ganymedes.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class PropertiesConfig {

    private static final String BASE_URL = "https://www.dofus.com";

    public String getBASE_URL() {
        return BASE_URL;
    }

}
