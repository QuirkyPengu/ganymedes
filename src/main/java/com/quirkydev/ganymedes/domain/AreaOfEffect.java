package com.quirkydev.ganymedes.domain;

/*

    == AreaOfEffect is a domain enumeration which specifies the type of Area of Effect ==

 */

public enum AreaOfEffect {
    CIRCLE, SQUARE, LINE
}
