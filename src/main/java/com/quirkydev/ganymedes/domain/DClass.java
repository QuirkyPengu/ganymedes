package com.quirkydev.ganymedes.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
/*

    == DClass is a domain class of a dofus class ==

 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DClass {

    private int id;

    private String shortName;
    private String longName;
    private String archetype;
    private String description;
    private String[] roles;
    private String[] roleDescriptions;

    private int[] vitalityCaps;
    private int[] intelligenceCaps;
    private int[] strengthCaps;
    private int[] agilityCaps;
    private int[] chanceCaps;
    private int[] wisdomCaps;

}
