package com.quirkydev.ganymedes.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/*
    == DSpell is a domain class of a dofus spell ==

    ap_costs, levels, normal_effects, critical_effects, minimum_range,
    maximum_range, sizes_area_of_effect, casts_per_turn_per_target,
    casts_per_turn, turns_between_casts:
    - Sizes can be 1 (variant) and 3 (original)

    type_area_of_effect:
    - Determines the type of Area of Effect

    classId is -1:
    - Spell does not belong to class

    variantId is -1:
    - Spell does not have a variant

    variantId is 0:
    - Spell is a variant to an original spell

    originalId is -1:
    -Spell does not have an original

    originalId is 0:
    -Spell is an original to a variant spell
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DSpell {

    private int id;
    private String url;

    private String name;
    private String description;
    private int[] ap_costs;
    private int[] levels;
    private String[] normal_effects;
    private String[] critical_effects;
    private int[] minimum_range;
    private int[] maximum_range;
    private boolean modifiable_range;
    private boolean line_of_sight;
    private boolean linear;
    private boolean diagonal;
    private boolean needs_target;
    private AreaOfEffect type_area_of_effect;
    private int[] sizes_area_of_effect;
    private int[] casts_per_turn_per_target;
    private int[] casts_per_turn;
    private int[] turns_between_casts;

    private int classId;
    private int variantId;
    private int originalId;

}
