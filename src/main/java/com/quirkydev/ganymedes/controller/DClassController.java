package com.quirkydev.ganymedes.controller;

import com.quirkydev.ganymedes.config.BeanConfig;
import com.quirkydev.ganymedes.domain.DClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/class")
public class DClassController {

    private final BeanConfig beanConfig;

    @Autowired
    public DClassController(BeanConfig beanConfig) {
        this.beanConfig = beanConfig;
    }

    @GetMapping
    @ResponseBody
    public List<DClass> getAllDClasses() {
        List<DClass> result = new ArrayList<>();
        try {
            for (DClass dClass : beanConfig.getDofusClassService().getAllDClasses()) {
                result.add(dClass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public DClass getDClassById(@PathVariable int id) {
        DClass result = null;
        try {
            result = beanConfig.getDofusClassService().getDClassById(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
