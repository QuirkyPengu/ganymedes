package com.quirkydev.ganymedes.service;

import com.quirkydev.ganymedes.config.PropertiesConfig;
import com.quirkydev.ganymedes.domain.DClass;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DClassService {

    private String overviewURL = "/en/mmorpg/encyclopedia/classes";
    private String breedSectionClass = "ak-breed-section";

    @Autowired
    private PropertiesConfig propertiesConfig;

    public List<DClass> getAllDClasses() throws IOException {
        List<DClass> dClasses = new ArrayList<>();
        Element mainElement = getMainElement(propertiesConfig.getBASE_URL() + overviewURL);
        for (Element classElement: mainElement.getElementsByClass(breedSectionClass)) {
            Element classATag = classElement.select("a").first();
            String url = classATag.attr("href");
            DClass dClass = getdClass(url);
            dClasses.add(dClass);
        }
        return dClasses;
    }

    public DClass getDClassById(int id) throws IOException {
        Element mainElement = getMainElement(propertiesConfig.getBASE_URL() + overviewURL);
        for (Element classElement: mainElement.getElementsByClass(breedSectionClass)) {
            Element classATag = classElement.select("a").first();
            String url = classATag.attr("href");
            if (url.contains("/" + Integer.toString(id) + "-"))
                return getdClass(url);
        }
        return null;
    }

    private DClass getdClass(String url) throws IOException {
        DClass dClass = new DClass();
        dClass.setId(getId(url));
        dClass.setShortName(getShortName(url));
        Element mainBreedElement = getBreedElement(propertiesConfig.getBASE_URL() + url);
        dClass.setLongName(getLongName(mainBreedElement));
        dClass.setArchetype(getArchetype(mainBreedElement));
        dClass.setDescription(getDescription(mainBreedElement));
        dClass.setRoles(getRoles(mainBreedElement));
        dClass.setRoleDescriptions(getRoleDescriptions(mainBreedElement));
        dClass.setVitalityCaps(new int[] {9999, -1, -1, -1});
        int[] elementalCaps = new int[] {100, 200, 300, 9999};
        dClass.setIntelligenceCaps(elementalCaps);
        dClass.setStrengthCaps(elementalCaps);
        dClass.setAgilityCaps(elementalCaps);
        dClass.setChanceCaps(elementalCaps);
        dClass.setWisdomCaps(new int[] {-1, -1, 9999, -1, -1});
        return dClass;
    }

    private Element getMainElement(String url) throws IOException {
        Document classesDoc = Jsoup.connect(url).get();
        return classesDoc.select("main").first();
    }

    private Element getBreedElement(String url) throws IOException {
        Document classDoc = Jsoup.connect(url).get();
        return classDoc.getElementsByClass("ak-breed-before-content").first();
    }

    private int getId(String url) {
        return Integer.parseInt(url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("-")));
    }

    private String getShortName(String url) {
        return url.substring(url.lastIndexOf("-") + 1).toLowerCase();
    }

    private String getLongName(Element element) {
        Element longNameElement = element.getElementsByClass("ak-breed-long-name-content").first();
        return longNameElement.selectFirst("span").text();
    }

    private String getArchetype(Element element) {
        Element descriptionElement = element.getElementsByClass("ak-breed-description").first();
        Element archetypeElement = descriptionElement.selectFirst("span");
        return archetypeElement.text().length() > 0 ? archetypeElement.text() : null;
    }

    private String getDescription(Element element) {
        Element descriptionElement = element.getElementsByClass("ak-breed-description").first();
        Element archetypeElement = descriptionElement.selectFirst("span");
        return descriptionElement.text().length() > 0 ? descriptionElement.text().substring(archetypeElement.text().length() > 0 ? archetypeElement.text().length() + 1 : 0) : null;
    }

    private String[] getRoles(Element element) {
        Element rolesElement = element.getElementsByClass("ak-breed-roles").first();
        String[] roles = new String[3];
        int i = 0;
        for (Element roleElement : rolesElement.select("span.ak-breed-role-icon")) {
            roles[i] = roleElement.text();
            i++;
        }
        return roles;
    }

    private String[] getRoleDescriptions(Element element) {
        Element roleDescriptionsElement = element.getElementsByClass("ak-breed-roles").first();
        String[] roleDescriptions = new String[3];
        int i = 0;
        for (Element roleDescriptionElement : roleDescriptionsElement.select("script")) {
            String script = roleDescriptionElement.html();
            roleDescriptions[i] = script.substring(script.indexOf(">") + 1, script.lastIndexOf("<"));
            i++;
        }
        return roleDescriptions;
    }
}
