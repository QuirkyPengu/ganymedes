package com.quirkydev.ganymedes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GanymedesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GanymedesApplication.class, args);
	}
}
